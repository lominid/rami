# Rami

Rami is another permutation of my engelang skeleton.

Here are its notable features:

* Open syllable structure.
I've made conlangs with open syllable structures before but they sounded crap.
* Stress based parts of speech.
* Relatively flexible word order.

Here are its main pitfals:

* The morphology is fairly basic.
* There are alot of minimal pairs.
* Grammatical stress is hard to hear.
I don't know whether it's unfamiliarity or practical impossibility.
It would take quite a while to distinguish word boundaries without stress on each word if possible.
* Doesn't contain much in the way of semantics like Latejami.

## Morphophonology

```
S = p d g l r n pf ts q/ʒ/
G = a i u ai au
V = a i u
F = s f j /ʃ/ b t k m z v c/tʃ/ h/h~ʔ/
E = b k t m

Morpheme = SG (SG)? (FV)* EV (FV)*
Prefix = SG (F|nf)V
Root = Prefix* Morpheme
Particles = EG (FV)*
```

This yields 360 two syllable roots and 1080 three syllable derivatives.
While reading, three syllable minimal pairs shouldn't be too hard to distinguish, as they're related to more distinct two syllable roots.

V, z, q, ts, pf and c aren't used natively (for now™) but are useful in loanwords.

### A note on orthography
If you don't have diatrics available, the accents can be written as double letters and the prenasalisation can be written as an 'm' at the end of the word.
In the diphthongs of 'a', only the 'a' is doubled.
Any null onset particles are written with h as an onset.

## Grammar

### Nasal and stress based inflections

The syllables of nouns are not stressed.

`Nama = animal`

Verbs are stressed on the first syllable.

`Gúta nama = the animal eats`

Adjectives are stressed on the last syllable.

`Gúta pakì nama = the old animal eats`

Custom conjunctions prenasalise the following word's onset.

`Gúta pakì nama patā gabù nama = the old animal goes before the young animal`

### Adjectives

In a situation where grouping matters, adjectives will group rightwards.
Though such a thing shouldn't really happen.
Unlike languages such as Lojban with 'tanru' and moreso Toaq with serial verbs, all adjectives in Rami merely predicate on their noun.

`Ratì damu = red building`

Adjectives can take arguments.

```
Pubì ha rasatu nama = war loving animal
Laumì ha bi nísiba piba = sleepy person (lit. want-to-sleep person)
```

Adjectives placed before a verb act as though they consume that verb's clause.

`Patà ráti damu = páta ma damu ráti = the house was red`

These adjectives can also take arguments

`Patà ha rasatu ráti damu = before the war, the house was red`

### Clauses and sentences

All clauses, excpet for the initial sentence level clause are head final.
This is syntactically safe, as the sentence level clause verb is always next to an implicit starter or an explicit one.
The sentence level clause is implicitly ended by either the starter of the next sentence or by a subordinating verb.

This means that if you place a verb after a sentence, the initial sentence behaves as though it
were a clause.
This process can continue indefinitely.

This is useful for alot of things, namely tense.

```
Gúta nama = the animal eats
Gúta nama páta = the animal ate
```

Sentence level clauses can also assume a head final form.

`Mu damu ráti = (starter) the house is red`

In a clause: `Ma damu ráti = that the house is red`

In a relative clause: `Damu mi ráti = the house that is red`

A property: `Rába damu bi tai ráti = the hous stops being red (lit. the house starts being not-red)`

Adjectives can precede any clause.
When this happens, the meaning is the same as if the adjective preceded the head verb.
See above if you've forgotten.

Clause openers take stress to turn them into verbs.

`Má rama gama rátu rasatu = The war is that I fight you`

Theoretically they could also have two stress points to turn them into adjectives.

`Ma rama gama rátù rasatu = The war (that is/where) I fight you`.

### Adder phrases

Any root can have it's leftmost argument filled in using an adder phrase.

I've already demonstrated this property with adjectives, so here I'll show how it works with verbs and nouns.

Using an adder phrase with a verb is essentially a way of changing the word order.
You may do this for stylistic reasons, mere preference or perhaps it makes the sentence more terse.

Here's a list of the same sentence permuted differently with and without adder phrases.

```
SOV: nama nabi gúti
SVO: Nama gúti ha nabi
VSO: Gúti nama nabi
VOS: Gúti ha nabi nama
```

Some verbs, when used as nouns, benefit from the ability to easily specify the object.

`Nimù líbu rama libu ha rama = I must help the one that helps me`.

### Relative clauses

Relative clauses are created using the particle 'mi'.
The relative particle is 'ta'.
They come after the noun.

`Nimù líbu rama piba mi ta rama líbu = I must help the person that helps me`

When a gaps are left in the clause, the relative particle is assumed to be in the leftmost gap.

`Nimù líbu rama piba mi líbu ha rama = I must help the person that helps me`

When relative clauses are used with verbs, the relative particle refers to the verb's clause.

### The genetive

#### The problem
The genetive was to be formed by prefixing 'hu' to a root.
The result is a new predicate, which means `[] is of ...`.

`Níta rama hu lumu = I know of it`

The noun would be able to take adjective and noun inflections, while remaining a quantified noun semantically.
Consider:

```
Hu ráma damu = The house is mine
Núma rama hu gamà damu = I see your house
```

The noun would be quantifiable.

`Rífabi hu bu namà nabi tu = The food of all the animals is where`

Alas, this method would be impossible.
Why? Well, for one main reason, which alone necesitated a new system.

Consider:

`Níma hu ramà gunehi limi lumu`

You might think that this groups as follows:

`Níma [hu ramà] gunehi limi lumu = My dog moves from here to there`

But it actually groups as follows

`Níma [hu <ramà gunehi>] limi lumu = The dog that is me moves from here to there`

#### The solution
After an innumerable amount of paces to and from my swivel chair, I came around to the solution for the genetive problem.

The adjective form of the genetive would behave in an unorthodox manner.
Rather than most of the particles in Rami, it would become a postposition.

`Rama hù damu = my house`

Now any amount of left grouping genetive phrases could precede a head noun.

`Bi níba hù daumi hù dijimi = The valley of the shadow of death`

This method:

* Is elegant, requiring only one syllable to form the genetive.
* Doesn't alter the meter, a genetive adjective is still stressed before the noun.

The noun behaviour would then remain the same.

`Níta rama hu lumu = I know of it`

The verbal behaviour would also remain the same.

`Hu raikà píba damu = The house is of the angry person`

### Numebers and counting
The particle 'hi' starts a number phrase.
Number phrases are ended by a vowel change to either: 'a' or 'u'
'a' indicates quantity, while 'u' indicates the ordinal.

```
s = 0
t/d = 1
n = 2
m = 3
r = 4
l = 5
j/q = 6
g/k = 7
f/v = 8
p/b = 9

```

#### Some examples
tai = to the exponent of (at the end, essentially between two numbers)

`Imitaima = 3000`

bai = over (between two numbers)

`Ilibaina = 5/2`

sai = decimal point (between two numbers)

`Imitisaila = 31.5`

kai = reciprocal (prefix)

`Ikaima = 1/3`

rai/rau = separator

`Imitaimiraikaima = 3000 + 1/3`

lai = negative sign

`Ilaimi = -3`

As self segmentation isn't contingent on spaces or punctuation, you can include them in numbers without any change in meaning.

`I mitaimi rai lika = 3*10^3 + 57 = 3057`

### Letters and spelling
Spellings begin with an 'hau' and are closed witn an 'hi'.
As vowels are not a problem (there is an opener and a closer), we can have more characters.

```
Cu = letter.
HV = vowel.
Ci = number.
Ca = A letter that also represents the end of a spelling phrase, without the need of 'hi'.
Hai = I
```

Diphthongs are spelled as compound letters, 'hai' = 'hahai'.

```
Haupudufuhi = PDF
Hauduhoguhi = DOG
Hauluhainuhukusuhi = LINUKS
```

### The passive
The passive is formed simply by doubling the first syllable of a root.

```
guta = [] eats []
guguta = [] is eaten by []
```

But what about words with three places?
Well, just triple the first syllable...

Or use the temporary prefix 'misi'

### Anaphora
Rather than using a number of pronouns, use rami anaphora (like latejami).
To form anaphora, take the first syllable of the word you want to make and append 'hau'.
If the source word is a compound, take the syllable from the head noun.

`Gisami -> gihau`

If this syllable is a diphthong of 'a', the latter part is removed.

`Danfunauta -> nahau`

This possibly reduces the scope of loanwords, so I may remove it in the future.

### Secondary subordination

Normally, what I call subordination can only occur at the end of a sentence.
However, using 'bau', you can subordinate any closed clause.

`Nama mi rama núma bau páta = The animal that I saw`

The subordinating verb can take arguments, both in their regular form and as adder phrases.
The first argument of the subordinating verb is the clause that it is subordinating.

```
Nama mi rama núma bau ma rama ráratu páta = The animal that I saw before I was attacked
Nama mi rama núma bau páta ha ma rama rárata = The animal that I saw before I was attacked
```

### Those custom conjunctions

Custom conjunctions can be made out of a verb whose two arguments are clauses.
The resulting sentence is one where this verb predicates over two identical
clauses, except for the conjunct argument.
The first clause contains the left noun and the second clause contains the right

`Gúta pakì nama patā gabù nama -> Páta ma pakì nama gúta ma gabù nama gúta`

### Loanwords

Loanwords can be butchered in using the extra sounds mentioned in the morphophonology section.
Internal consonant clusters don't necessarily break the morphology, so long as consonants obey the structure S\*F\*E F\*.

Keep in mind that the value of a loanword system there in the hypothetical situation where the language has to be: a) parsed or b) processed from a soundbite.
Of course, the same could be said for conlangs as a whole, but sometimes it's just not worth the effort to transliterate a forgein word.

You could just put the word in quotation marks, followed by a root which describes what type of thing it is.

`Burger -> 'burger' nabi / purgarha`

#### Some transliterations

```
Shinar = Qinarha
Telephone = Delefonhi
Sargent = Tsarjenti
Telephone shop = Delefonhirubi
Dublin = Duplinhi
```

### Compunds

Compounds, like adjectives are right grouping.

Compounds are formed by prefixing the a version of a two syllable root onto another root.
This yields 360 prefixes.
To form the prefix version, convert the E consonant using these rules: `M>nf, b>f, t>s, k>j`.
Diphthongs are broken up by 'nf'.

```
Naufa = nau-fa (2 syllables)
Naunfa = na-un-fa (3 syllables)
```

Verbal stress goes on the first syllable of the head noun, and to form the passive of a compound, you duplicate the first syllable of the head noun. (This may change).

`Ranfirájaba rama daku = I translate the text`

Compounds are made with loanwords by changing the realisation last vowel.
`u->ø~ə, i->e, a->o`.

`Gunehirubi -> /guneherubi/ = dog-shop`

If the last part of the loanword is a diphthong, the diphthong is broken up.

`Pikaurubi -> /pikaərubi/ = pig-shop/butcher`

### Parsing logic

```
root-phrase = negative? root
	| numerical-root
	| seplling-root
	| BA root
	| anaphora

verb = róot-phrase
	| genetive-verb
noun = root-phrase
	| genetive-noun

sentence = verb-phrase noun-phrase* verb-phrase*
verb-phrase = adjective-phrase* verb adder-phrase?
adjective-phrase = adjective a-phrase?
adder-phrase = HA noun-phrase
noun-phrase =
	genetive-adjective-phrase? quantifier? adjective-phrase* noun (adder-phrase | relative-clause)? (conjunction noun-phrase)?
	| clause
genetive-adjective-phrase = noun-phrase HÙ
clause = MA noun-phrase* verb-phrase subordinating-part-of-clause?
subordinating-part-of-clause = BAU noun-phrase* verb-phrase subordinating-part-of-clause?
relative-clause = MI noun-phrases* verb-phrase

utterance = (( ^ | starter ) ~ sentence )*
```

[The pest.rs parser (probably outdated)](parser)

Copy the parser text into [pest.rs](https://pest.rs) to see it work.


#### Some things to note

You can't have both a relative clause and an inserted noun.

The parsing logic doesn't yet account for hierarchical structure.

## APPENDIX A - Lexicon

### Primary roots
```
daba = [] is disease
dabi = [] is [] enough to do []
dabu = [] is the end of []
daiba = N/A
daika = N/A
daima = [] is a coin
daita = N/A
daka = [] is positive
daki = [] covers []
daku = [] is a text
dama = [] is at present/now/at the same time as []
dami = [] is a bird
damu = [] is a building
data = [] is a system
dati = [] is a day
datu = N/A
dauba = N/A
dauka = [] sits on []
daumi = [] is a shadow
dauta = N/A
diba = [] is a barrier/wall
dibi = [] would be the case if [] were the case
dibu = [] is a professional
dika = [] is a destination
diki = [] is sharp
diku = [] hopes/pines for []
dima = [] likes []
dimi = [] is a hole/recess in the ground
dimu = [] is possible
dita = [] is a hand
diti = N/A
ditu = N/A
duba = [] is on the right
dubi = [] is wrong
dubu = [] moves through []
duka = [] falls from []
duki = [] is a head
duku = [] loses []
duma = [] is a 2D shape
dumi = [] is wicked/evil/cursed
dumu = [] lasts for [] amount of time
duta = [] occurs as quickly as []
duti = [] is behind []
dutu = N/A
gaba = [] is about/pertain to []
gabi = [] is a result
gabu = [] is new/young
gaiba = N/A
gaika = N/A
gaima = N/A
gaita = N/A
gaitu = [] smells []
gaka = N/A
gaki = [] is green
gaku = [] is contrasting or 'other'
gama = YOU 2ps.sing
gami = [] is a sound
gamu = [] causes []
gata = [] is 'just'/only true
gati = [] is nice
gatu = [] is a door/portal/gate
gauba = N/A
gaubi = [] does [property] again
gauka = N/A
gauma = N/A
gauta = N/A
giba = [] writes [] is far from []
gibi = [] is a property/characteristic/aspect
gibu = [] is more true than []
gika = [] is a town/settlement/village/city
giki = [] is a device for doing [property]
giku = [] is a circle
gima = [] is a color
gimi = [] is information
gimu = [] gives []
gita = [] is below []
giti = [] is rich in property []
gitu = [] is a large domesticated animal
guba = [] is a group
gubi = [] is a light source
gubu = [] is a mammal
guka = [] is the way in which [] is done/becomes true
guki = [] is in the east
guku = [] is a rock/stone
guma = [] is alone
gumi = [] is liquid stuff
gumu = [] waits for []
guta = [] eats []
guti = [] is God
laibi = [] is oil
gutu = [] is a cup
naimu = [] is a some kind of acquaintance of []
laba = [] is clean
labi = [] is white/bright
labu = [] loves []
laiba = N/A
laika = N/A
laima = [] is confused
laita = N/A
laka = [] is broad/wide/thick
lafaka = [] is far from []
laki = [] makes []
laku = [] is cold
lama = [] is fuel
lami = [] is small
lamu = [] is true
lata = [] is a gas
lati = [] is above []
latu = [] wants [] to be the case
lauba = [] is far away from []
lauka = N/A
lauma = N/A
lauta = [] is heavy
liba = [] is a swelling
libi = [] is free
libu = [] helps/benefits/aids []
lika = [] is true if [] is true/[] is necessary for []
liki = [] is a piece/part/element/quantum of []
liku = [] is a tube/tunnel
lima = [] is water
limi = [] is here/this
limu = [] controls []
lita = [] is a path/road
liti = [] is to the left
litu = N/A
luba = [] is a man-made place
lubi = [] is negative
lubu = [] is normal
luka = N/A
luki = [] is true because of []
luku = N/A
luma = [] excretes []
lumi = [] is the moon
lumu = [] is there/that
luta = [] is the corporeal element of []
luti = [] is the purpose of []
lutu = N/A
naba = [] is a child
nabi = [] is a food item
nabu = [] is a room
naibu = [] is in front of []
naika = N/A
naima = N/A
naita = N/A
naka = [] is a victim
naki = [] acquires []
naku = [] is a tool/implement
nama = [] is an animal
nami = [] is the target of []
namu = [] is a year
nata = [] is a point in time
nati = [] is a net
natu = [] is what it is
nauba = [] trusts []
nauka = [] is able to do []
nauma = [] is an example of []
nauta = [] is sticky/adhering
niba = [] collapses/dies
nibi = [] is a tree/woody plant
nibu = [] is a branch of science concerned with []
nika = [] is grey
niki = [] is solid
niku = [] is a reptile
nima = [] is after []
nimi = to be in motion
nimu = [] must occur
nita = [] knows []
niti = [] communicates with []
nitu = [] is before [] in a sequence
nuba = [] meets []
nubi = [] is meat
nubu = [] is sad
nuka = [] is a piece of furniture
nuki = [] is less true than []
nuku = [] is similar to []
numa = [] perceives []
numi = [] is thankful to []
numu = [] refers to more than one thing
nuta = [] is a sport
nuti = [] is a number
nutu = [] sleeps with []
paba = N/A
pabi = [] is a seed
pabu = N/A
paiba = N/A
paika = N/A
paima = [] is a friend of []
paita = [] follows []
paka = N/A
paki = [] is old
paku = N/A
pama = N/A
pami = [] does [] wholly
pamu = [] is inside []
pata = [] is before []
pati = [] is an item of clothing
patu = [] utters []
pauba = N/A
pauka = [] guides [] in way [property]
pauma = [] feels comfortable doing [property]
pauta = N/A
piba = [] is a person
pibi = [] laughs
pibu = [] serves function/role []
pika = [] is happy
piki = [] is strange
piku = [] is a weapon
pima = [] is the same as []
pimi = [] is in the north
pimu = N/A
pita = [] is yellow
piti = [] is before [] in a sequence
pitu = [] is an event
puba = N/A
pubi = [] loves []
pubu = [] is purple
puka = [] fears/is afraid of []
puki = [] is a little bit [property]
puku = N/A
puma = [] uses []
pumi = [] loves []
pumu = N/A
puta = [] is a computer
puti = [] sings []
putu = [] is a foot
raba = [] becomes/changes into []
rabi = [] is worth []
rabu = [] is large
rajabu = [] is so [property] that they satisfy [property]
raiba = N/A
raika = [] is angry at []
raima = N/A
raita = N/A
raka = [] is fire/flame
raki = [] is a plant
raku = [] avoids []
rama = ME 1ps.sing
rami = [] is a lanugage
ramu = [] is probably true if [] i true
rata = [] is art
rati = [] is red
ratu = [] attacks []
raubi = [] returns to doing []
rauka = N/A
rauma = [] is rain []
rauta = [] is still true
riba = [] finds []
ribi = [] is a natural location
ribu = [] holds []
rika = [] is a weather condition
riki = [] is an insect
riku = [] is a parent
rima = [] is calm/peaceful/safe
rimi = [] wears []
rimu = [] is a world/domain
rita = [] is warm
riti = [] is important/significant
ritu = [] is royal
ruba = [] is healthy
rubi = [] is a shop/market
rubu = [] is a sibling
ruka = [] is crushed material
ruki = [] is narrow
ruku = [] is a vehicle
ruma = [] is a surface
rumi = [] thinks []
rumu = [] is a book
ruta = [] originates in []
ruti = [] is a window
rutu = [] is eternal/infinite
```

### Secondary Roots
```
dasiba = [] is sick
dafaba = [] curses []
dajiba = [] is a poison
dafabu = [] stops doing [property]
dajiki = [] is a blanket/duvet
dajiku = short story
dafuku = [] is a document
dasuku = [] reads []
dafami = [] flies
dajimi = [] is the sky
dasimi = [] is blue
dajimu = [] is a complex
dafumu = [] is a house
dajata = [] is a governmental system
dafata = [] organises/prepares []
dasita = [] is ordered/structured/orderly/logical
dasiti = [] is popular/en vouge among []
daujata = [] is folded/bent around []
disiba = [] is closed/locked
dijibu = [] is a job
difabu = [] works
difaka = [] points towards []
dijiki = [] is a knife
difaki = [] cuts []
disimu = [] is open/unlocked
difata = [] catches []
disuta = [] can reach/touch []
dujibi = [] is true instead of [] being true
dufaka = [] lowers/descends upon []
dufuki = [] is a face
gujika = [] is an explanation
gufaka = [] does [property] in way []
dusiku = [] is mourning the loss of []
dufuma = [] is a 3D shape
dufami = [] loathes/hates/despises []
dujimi = [] is excrement/crap
dujuta = [] occurs as slowly as []
gasiba = [] is relevant.
gasibi = [] is efficient/prosperous
gausibi = [] is extra/additiona/auxhiliary
gajibu = [] is early
gaisatu = [] is a nose
gafaku = [] disobeys/commits treason against []
gasiku = [] is specific/distinct/particular among []
gajama = 2ps.pl
gasita = [] is pendantic
gajuti = [] is not nice
gafati = [] dwells/abodes in []
gijubu = [] increases in how much it does []
gisika = [] is municipal
gijika = [] is a council
gijiku = [] is a ring
gifaku = [] surrounds []
gisiku = [] is round
gifuku = [] is a ball
gifutu = [] is a pasture
dijimi = [] is a valley
gasima = [] is colorful
gifami = [] learns
gisami = [] is a school
gijita = [] is the earth/soil
gisita = [] is brown
gifata = [] burries []
gufuba = [] is among  []
nufabu = [] has mercy on []
gujaba = [] is with []
gujibi = [] is the sun
gufabi = [] shines on []
gusibi = [] is bright/rich in light.
gufubu = [] is hair
gujibu = [] is mulk
gujuki = [] is in the west
gusiku = [] is hard
gujiku = [] is a brick
gufami = [] drinks []
gujata = [] gorges on []
gusiti = [] is holy
gufuti - [] is a creed/faith
gusati - [] is a church
lajuba = [] is dirty
lasiba = [] is modest/good/holy
lajibi = [] is the truth
laifama = [] confounds/disorganises []
lafuki = [] is a clone
lajima = [] is oil
lafumu = [] is a fact.
lafati = [] jumps/leaps []
lasati = [] is the paragon/summit/top of []
lausita = [] is sturdy/well built/reliable
laujata = [] is overly/too/excessively [property] to do [property]
lijiba = [] is a cancerous growth
lijiki = [] is a dot/point
lifuki = [] is a body part
lisima = [] is wet
lijima = [] is a cloud
lifuma = [] is alcohol
lijami = 3ps.pl
lifajita = [] is a choice
lifata = [] choses []
lijata = [] is a fork
lufumi = [] is a month
lusimi = [] is dark/black
lujami = [] is the cosmos
lujamu = 3ps.pl
lufati = [] is successful thanks to []
nasiba = [] is foolish/idiotic
nafaba = [] gives birth to []
nasabu = [] is a hotel/hostel
najika = [] is an injury
najiki = [] is a gift/present
nafaki = [] awards/endowsn [] upon []
najiku = [] is a component
nasima = [] is alive
najima = [] is a dog
najiti = [] is a basket
najitu = [] is the news
naufaka = [] is adept at doing []
nausuma = [] backs/is evidence of []
nisuba = [] is death
nifaba = [] sleeps
nisiba = [] is lazy/lethargic
nijibi = [] is wood
nisaki = [] is the floor/ground
nifaku = [] crawls
nifuma = [] is posterity
nifami = [] moves
nijami = [] ambulates/walks/runs
nisumi = [] sends [] to []
nifumu = [] is a prophecy
nijimu = [] is a law
nisita = [] is wise
nujibi = [] is muscle
nusaka = [] is a fixture
nusiki = [] is weak/sickly
nujiki = [] is a detail
nufiki = [] decreases in how much it does []
nujiku = [] is a version/variant/model/kind/type
nufuma = [] is an image/sight
nujima = [] is a mirror
nusima = [] is blatant/obvious/evident
nufamu = [] measures/records []
nusata = [] is a court/field/plain
nufata = [] plays with []
nujita = [] is a competition/match/game
nusitu = [] is attractive
pajiki - [] is outdated/old fashioned/an anachronism
pafaki - [] remembers []
pasimi = [] is full
pafumu = [] a core/center/middle of []
pafiti = [] is creased
pajitu = [] is a name
pijiba = [] is mankind/people in general
pisibi = [] is funny
pijama = [] is in balance with []
pijumi = [] is in the south
pujima = [] is a method
pujika = [] is a ghost/apparition/spirit
pusama = [] is a workshop
pujimi = [] is a darling/loved one
pusuki = [] is a song
pujitu = [] is a leg
rajaba = [] changes [] in way [property]
rajibi = [] is currency
rafubi = [] is gold
rasibi = [] is expensive
rajubi = [] is frugal
rafabi = [] buys/purchases []
raijaka = [] screams at []
rasuka = [] burns []
rajiki = [] is a flower
rajama = 1p.pl
rajimi = [] is a word
rasami = [] is a tounge
rajata = [] is a collection
rafuta = [] is a pattern/filgree/design
rafuti = [] is blood
rasatu = [] is war
raujima = [] is a drop
raufuma = [] is a flood
rausima = [] flows
rausita = [] persists
rijaba = [] searches for []
rifuba = [] is a discovery/innovation
rijibi = [] is a country/land
risibi = [] is wild/undomesticated
rifabi = [] is at []
rijibu = [] is a container
rifubu = [] has []
rijika = [] is the air/atmosphere
rifaka = [] breathes/inhales
risaka = [] is outside []
rifaki = [] annoys []
rijiku = [] is a married couple
rifaki = [] marries []
rijita = [] is fire/flame
rijatu = [] reigns over []
rujika = [] is sand
rufuka = [] is salt
rufuki = [] is next/near to []
rujiki = [] is a string/rope
rusiki = [] is stiff/constrained/limited in aspect []
rusifuki = [] is the limit
rujiku = [] is a horse
rufama = [] is on []
risima = [] is flat
rijama = [] reclines on []
rujima = [] is the horizon.
rujimi = [] is a thought/idea
rusami = [] is the mind
rujimu = [] is a chapter
rujita = [] is the beginning of []
rufata = [] starts to do []
```

### Compounds
```
gaufilajirami = [] is an auxlang
lajirami = [] is a conlang
lanfigisudibu = [] is a sheperd
lausarausima = [] overflows
dunfinaimu = [] is an enemy of []
daujanuku = [] is a chair
gusigimu = [] annoints []
runfanuku = [] is a table
gusinauba = [] believes in []
ranfirajaba = [] translates []
pifurumi = [] is sane
nusinujiki = [] is a statistic, nusinujikisi = [] is statistical
nisiputa = [] is a phone
danfunauta = [] is morter
pasiputa = [] is a laptop
nunfanifaba = [] dreams []
nisanifaba = [] lucid dreams []
lunfilabi = [] is grey
lusinita = [] understands []
lausagati = [] is salubrious/too good to be real or true
paufama = [] assuages/calms/comforts/cools []
runfulubu = [] is a library
ranfinibu = [] is linguistics
lajarajaba = [] scatters []
```

### Loanwords
```
lefantu = [] is an elefant
leku = [] is a horse
gunehi = [] is a dog
gauhi = [] is a cow
pikau = [] is a pig
padata = [] is a potato
lojiba = [] is lojban
papelha = [] is Babel
```

#### Compounds with loanwords
```
gunehirubi = [] is a dog shop
lijilojiba = [] is a gismu
```
## APPENDIX B - Particles

### Clauses
```
bi = property clause
mi = relative clasue
ma = clause opener
bau = subordination particle
```

### Quoting
```
ba = start gramatically correct name (doesn't need to take stress)
mauji = start name quote
mau = start quote
tasa = end quote (can take stress)
```

### Numbers
```
hi = start a number phrase, closed by a change to 'a' (quantitave) or 'u' (ordinal).

s = 0
t/d = 1
n = 2
m = 3
r = 4
l = 5
j/q = 6
g/k = 7
f/v = 8
p/b = 9

tai = to the exponent of (at the end, essentially between two numbers)
bai = over (between two numbers)
sai = decimal point (between two numbers)
kai = reciprocal (prefix)
rai/rau = separator
lai = negative sign
```

### Letters and spelling
```
Cu = letter.
HV = vowel.
Ci = number.
Ca = A letter that also represents the end of a spelling phrase, without the need of 'hi'.
Hai = I
```

### Conjunctions
```
ti = or
ku = and
kau = group and
```

### Quantifiers

Quantifiers come before the noun, and before any adjectives if adjectives are modifying that noun.
Genetive adjectives come before the quantifier.

```
bu = all
buji = some
ki = no
ka = only
kai = definite
tu = question (which ...)
mai = generic
```

### Other
```
ta/tau = propety/antecedent pronoun
misi = x1 \<-\> x2
tai = negative, placed before a root
hu = make predicate which means 'of X'
ha = fills in the leftmost argument of the preceding morpheme
-hai = given either a prefix to form an interjection OR a simple syllable (closed class).
-hau = forms anaphora
```

### Sentence starters
```
mu = default starter
bai = order
hai = informal address
taji = ultimately
tafu = questioning starter
bija = therefore/in this case
masu = but/however/alas
miji = in other words
```

## APPENDIX C - Affixes (are infixed to roots and appended to compounds)
Each word ought to have no more than three derivatives.
This is to prevent excessive minimal pairs.

Derivative 1

* ja = relation
* ju = adjective
* ji = noun

Derivative 2

* sa = noun
* su = relation
* si = adjective

Derivative 3

* fa = relation
* fu = noun
* fi = adjective

Tense words use derivative 2 for the perfect, 1 for the habitual and 3 for the imperfect.

## APPENDIX D - Examples

### Nibà paima
```
Rama núbu ku dúsiku.
Masu dáma ma láumi ha,
bi paimà dita ríribu
ku rutù bi ta gújaba.
```

#### Note
These lines are best analysed as two unstressed syllables followed by two anapests.

Making this kind of 'poetry' in Rami is halfway between sticking to a meter in english and making a Toaq pangram.
While there is no grammatical tone, there is grammatical stress.
Gramatical stress is less like tone in that the word at a particular part of the text is not stuck to a single part of speech.
