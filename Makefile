default: folders tools
	cp rami.css build/
	pandoc rami.md -o build/rami.html -c rami.css
	tools/make_examples

tools:
	cp ~/bin/rami* tools/

clean:
	if [ -e build ] ; then rm -r build ; fi

folders:
	mkdir -p build
	mkdir -p build/examples
	mkdir -p build/examples/ascii
	mkdir -p build/examples/ipa
